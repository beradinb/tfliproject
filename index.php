<?php
  error_reporting(0);
  if (isset($_POST['search_city'])){

    $city    = $_POST['city']; //City entry from the user
    $api_key = 'e7e0a939518feeda5894dfa3c7a6bf4a';

    //URL for openweathermap api, including the city and api key
    $url  = "http://api.openweathermap.org/data/2.5/weather?q=".$city."&units=metric&cnt=7&lang=en&appid=".$api_key;

    //Retrieving JSON data
    $json = file_get_contents($url); //Getting all the json content
    $data = json_decode($json,true); //Decodes the json content
    //echo json_encode($data);

    /* WEATHER VARIABLES */
    $current_temp     =   round($data['main']['temp']);       //Current Temperature and rounding to nearest degree celcius
    $minimum_temp     =   round($data['main']['temp_min']);   //Minimum Temperature and rounding to nearest degree celcius
    $maximum_temp     =   round($data['main']['temp_max']);   //Maximum Temperature and rounding to nearest degree celcius
    // $sunrise       =   $data['sys']['sunrise'];            //Sunrise
    // $sunset        =   $data['sys']['sunset'];             //Sunset
    $wind_speed       =   $data['wind']['speed'];             //Wind Speed
    $humidity         =   $data['main']['humidity'];          //Humidity
    $weather_desc     =   ucwords($data['weather'][0]['description']); //Weather description (first character of each word to uppercase)
    $city_name        =   $data['name'];                      //City found
    $country_symb     =   $data['sys']['country'];            //Country of city found
    $full_city_name   =   $city_name. ', '. $country_symb;    //City name with country incase of duplicates
    $img_icon         =   "http://openweathermap.org/img/w/" .$data['weather'][0]['icon']. ".png"; //URL for weather icon

    // $sunrise = date('r', $sunrise);
    // $sunset = gmdate('r', $sunset);

    /*function convertToLocalTime($timestamp_json) {
      $timestamp  = date('r', $timestamp_json);
      $date       = new DateTime($timestamp);
      $date->setTimezone(new DateTimeZone("Europe/London"));
      return $date->format('H:i');
    }
    function convertToLocalTime($timestamp_json) {
      $epoch = $timestamp_json;
      $dt = new DateTime("@$epoch");  // convert UNIX timestamp to PHP DateTime
      echo $dt->format('H:i');
    }*/

    //Function to convert the windspeed to mph
    function windspeedToMPH ($windspeed) {
      $mphspeed = $windspeed / 0.44704;
      return round($mphspeed);
    }

  }
?>

<!DOCTYPE HTML>
<html>
<head>
<title>Weather API</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

<!-- CSS -->
<link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
<link rel="stylesheet" href="css/style.css">

<!-- JS -->
<link rel="stylesheet" href="bootstrap/js/bootstrap.min.js">

</head>

<body class="overflow-hidden">
  <div class="container">
    <div align="center">

    <div class="title">Weather API</div>

      <!--Search city form-->
      <form class="search-form" method="post"  onsubmit="return FormValidation()" align="center">
         <div class="input-group">
           <label class="col-12 city_label" for="city" align="center">Enter Location Name :</label>
           <input class="form-control text_area" id="searchTextField" name="city" placeholder="Enter a location e.g. Manchester, UK">
           <span class="input-group-btn">
             <button class="btn btn-success search_btn" type="submit" name="search_city">Go</button>
           </span>
         </div>
         <div class="not_found" id="error_message"align="center"></div>
      </form>

      <!--Weather data ouput-->
      <div class="information_area">
      <?php if ($data != null) : ?>
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 current_temp_div">
          <h2 class="current_city"><?php echo $full_city_name ?></h2>
          <h4 class="current_desc"><?php echo $weather_desc ?></h4>
          <h2 class="current_temp"><?php echo $current_temp ?>&#176;C</h2>
          <img src="<?php echo $img_icon?>" width='80' height='80'>
        </div>

        <div class="row other_info" align="center">
          <h4 class="col-12 wind_speed">Wind speed: <?php echo windspeedToMPH($wind_speed); ?> MPH</h4>
          <h4 class="col-12 humidity">Humidty: <?php echo $humidity ; ?>%</h4>
        </div>

        <!--<div class="row sun_times">
          <h4 class="col-lg-6 col-md-6 col-sm-6 col-xs-12 sunrise" align="right">Sunrise: <?php echo convertToLocalTime($sunrise); ?></h4>
          <h4 class="col-lg-6 col-md-6 col-sm-6 col-xs-12 sunset" align="left">Sunset: <?php echo convertToLocalTime($sunset); ?></h4>
        </div>-->

      <?php endif ?>
      <?php if ($data == null && isset($_POST['search_city'])) : ?>
        <div class="not_found" align="center">Not found, try again.</div>
      <?php endif ?>
      </div>

    </div>
  </div>
</body>

</html>

<script>
  //Check if text field is empty
  function FormValidation(){
    var cityfield = document.getElementById('searchTextField').value;
    if(cityfield == ""){
          //alert('Please Enter City Name');
          document.getElementById('searchTextField').style.borderColor = "red"; //Red border around text field
          document.getElementById('error_message').innerHTML= "Enter a city."; //Error message
          return false;
      }
  }
</script>
